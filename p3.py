import time

if __name__ == '__main__':

    t1 = time.time()

    with open('words.txt') as fs:
        word_list = fs.readlines()

    word_list = list(map(lambda w: w.strip(), word_list))
    word_set = set(word_list)

    for word in word_list:
        reversed_word = word[::-1]
        if reversed_word != word and reversed_word in word_set:
            print(word, reversed_word)

    t2 = time.time()
    print('\n', t2 - t1, 's')
