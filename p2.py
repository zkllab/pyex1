from datetime import datetime
from datetime import timedelta

if __name__ == '__main__':

    while True:
        try:
            date_raw = input('请输入日期时间(YYYY-MM-DD HH:MM:SS): ')
            date = datetime.strptime(date_raw, '%Y-%m-%d %H:%M:%S')
            date += timedelta(seconds=1)
            print(date)
        except ValueError:
            print('格式有误或日期不存在！')

# 测试用例参考：
# 2020-02-02 23:59:59
# 2020-02-30 23:33:33
